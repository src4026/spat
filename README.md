Student Performance Analysis Tools (SPAT)
======

***WIP!***

About
------
This is my dig at:
- What if there was a tool that evaluated a student's performance and displayed it as a line graph for scores gained during examinations? (Bonus: if the examination is made such that segregating it into its various aspects, such as logic, etc., a report can be formed for the students performance!) And
	- a comparison with other subjects of the same student to elucidate a trend in the various subjects (could be tied to other factors).
	- a comparison between students (including the nuance) to seek out the reason of the differences in performance and improve. This can be paired with the "Bonus" in the first point.
- And into a radar/spider chart.
- *But* with also the added liveliness of animations using [manim](https://github.com/ManimCommunity/manim)!

License
------
This project is licensed under the [GNU General Public License v3.0](LICENSE).
