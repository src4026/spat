from manim import *
import random

class PerformanceGraph1(Scene):
    def construct(self):
        chart = BarChart(
            values=[19.5, 18.25, 20, 20, 20, 20],
            bar_names=["English", "Kannada", "Hindi", "Mathematics", "Science", "Social Science"],
            y_range=[0, 20, 2],
            y_length=5,
            x_length=11,
            x_axis_config={"font_size": 36},
        )

        chart_labels = chart.get_bar_labels(font_size=48)

        chart_title = Tex("James' performance in PA-2 (2023)").next_to(chart, DOWN)

        self.play(DrawBorderThenFill(VGroup(chart, chart_labels, chart_title)))
        self.wait(2)

config.frame_height = 7
config.frame_width = 22

def create_line_graph(self, color):
    axes = Axes(
        x_range=[0, 33, 1],
        y_range=[0, 20, 2],
        x_length=20,
        y_length=4,
        x_axis_config={"numbers_to_include": np.arange(0, 34, 1)},
        y_axis_config={"numbers_to_include": np.arange(0, 21, 2)},
        tips=False,
    )

    labels = axes.get_axis_labels(
        x_label=Tex("Student Roll Number"), y_label=Tex("Marks")
    )

    x_vals = [x for x in range(1, 34)]
    y_vals = [random.randint(10, 20) for y in range(0, 34)]

    graph = axes.plot_line_graph(x_values=x_vals, y_values=y_vals, line_color=color)

    return axes, labels, graph
class PerformanceGraph2(Scene):
    def construct(self):
        axes1, labels1, graph1 = create_line_graph(self, YELLOW)
        self.play(DrawBorderThenFill(VGroup(axes1, labels1)))
        self.play(DrawBorderThenFill(graph1), run_time=5)

        axes2, labels2, graph2 = create_line_graph(self, BLUE)
        lg1 = VGroup(axes1, labels1, graph1)
        lg2 = VGroup(axes2, labels2, graph2)
        self.play(ReplacementTransform(lg1, lg2), run_time=3)
        self.wait(2)
        self.play(FadeOut(lg2))


