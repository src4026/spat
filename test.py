from manim import *

class UserInputExample(Scene):
    def construct(self):
        # Get user input
        user_input = input("Enter something: ")

        # Display the user input
        text = Text(f"You entered: {user_input}")
        self.play(Write(text))
